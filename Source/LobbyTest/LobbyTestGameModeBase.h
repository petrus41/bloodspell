// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LobbyTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LOBBYTEST_API ALobbyTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

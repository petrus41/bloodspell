// Copyright Epic Games, Inc. All Rights Reserved.

#include "LobbyTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, LobbyTest, "LobbyTest" );
